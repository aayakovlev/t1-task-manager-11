package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.List;

public final class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository repository;

    public ProjectServiceImpl(ProjectRepository repository) {
        this.repository = repository;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return repository.create(name, description);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public Project findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        final int recordsCount = repository.count();
        if (index == null || index < 0 || index > recordsCount) return null;
        return repository.findByIndex(index);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final int recordsCount = repository.count();
        if (index == null || index < 0 || index > recordsCount) return null;
        return repository.removeByIndex(index);
    }

    @Override
    public void save(final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
