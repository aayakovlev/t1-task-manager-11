package ru.t1.aayakovlev.tm.service;

import java.util.List;

public interface BaseService<T> {

    void deleteAll();

    List<T> findAll();

    T findById(final String id);

    T findByIndex(final Integer index);

    T removeById(final String id);

    T removeByIndex(final Integer index);

    void save(final T e);

}
