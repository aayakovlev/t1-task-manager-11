package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

public final class TaskServiceImpl implements TaskService {

    private final TaskRepository repository;

    public TaskServiceImpl(TaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return repository.create(name, description);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        final int recordsCount = repository.count();
        if (index == null || index < 0 || index > recordsCount) return null;
        return repository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final int recordsCount = repository.count();
        if (index == null || index < 0 || index > recordsCount) return null;
        return repository.removeByIndex(index);
    }

    @Override
    public void save(final Task task) {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
