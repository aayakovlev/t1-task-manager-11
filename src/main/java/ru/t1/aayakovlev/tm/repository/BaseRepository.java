package ru.t1.aayakovlev.tm.repository;

import java.util.List;

public interface BaseRepository<T> {

    int count();

    void deleteAll();

    List<T> findAll();

    T findById(final String id);

    T findByIndex(final Integer index);

    T remove(final T e);

    T removeById(final String id);

    T removeByIndex(final Integer index);

    T save(final T e);

}
