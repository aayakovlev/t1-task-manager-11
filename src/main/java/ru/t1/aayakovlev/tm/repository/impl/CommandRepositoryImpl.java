package ru.t1.aayakovlev.tm.repository.impl;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.model.Command;
import ru.t1.aayakovlev.tm.repository.CommandRepository;

public final class CommandRepositoryImpl implements CommandRepository {

    private static final Command ABOUT = new Command(
            CommandConstant.ABOUT, ArgumentConstant.ABOUT,
            "Show developer info."
    );

    private static final Command EXIT = new Command(
            CommandConstant.EXIT, null,
            "Exit program.");

    private static final Command HELP = new Command(
            CommandConstant.HELP, ArgumentConstant.HELP,
            "Show arguments description."
    );

    private static final Command INFO = new Command(
            CommandConstant.INFO, ArgumentConstant.INFO,
            "Show hardware info."
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConstant.PROJECT_CLEAR, null,
            "Clear project list.");

    private static final Command PROJECT_CREATE = new Command(
            CommandConstant.PROJECT_CREATE, null,
            "Create new project.");


    private static final Command PROJECT_LIST = new Command(
            CommandConstant.PROJECT_LIST, null,
            "Show project list.");

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            CommandConstant.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id.");

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CommandConstant.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index.");
    private static final Command PROJECT_SHOW_BY_ID = new Command(
            CommandConstant.PROJECT_SHOW_BY_ID, null,
            "Show project by id.");

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            CommandConstant.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index.");

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConstant.PROJECT_UPDATE_BY_ID, null,
            "Update project by id.");

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConstant.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index.");

    private static final Command TASK_CLEAR = new Command(
            CommandConstant.TASK_CLEAR, null,
            "Clear task list.");

    private static final Command TASK_CREATE = new Command(
            CommandConstant.TASK_CREATE, null,
            "Create new task.");

    private static final Command TASK_LIST = new Command(
            CommandConstant.TASK_LIST, null,
            "Show task list.");

    private static final Command TASK_REMOVE_BY_ID = new Command(
            CommandConstant.TASK_REMOVE_BY_ID, null,
            "Remove task by id.");

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            CommandConstant.TASK_REMOVE_BY_INDEX, null,
            "Clear task list.");

    private static final Command TASK_SHOW_BY_ID = new Command(
            CommandConstant.TASK_SHOW_BY_ID, null,
            "Clear task by id.");

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            CommandConstant.TASK_SHOW_BY_INDEX, null,
            "Clear task by index.");

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConstant.TASK_UPDATE_BY_ID, null,
            "Update task by id.");

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConstant.TASK_UPDATE_BY_INDEX, null,
            "Update task by index.");

    private static final Command VERSION = new Command(
            CommandConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command[] COMMANDS = new Command[]{
            ABOUT, EXIT, HELP, INFO,

            PROJECT_CLEAR, PROJECT_CREATE, PROJECT_LIST, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,

            TASK_CLEAR, TASK_CREATE, TASK_LIST, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,

            VERSION
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
