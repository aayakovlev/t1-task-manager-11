package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskControllerImpl implements BaseController {

    private final TaskService service;

    public TaskControllerImpl(TaskService service) {
        this.service = service;
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR TASKS]");
        service.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        final Task task = service.create(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.removeById(id);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.removeByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    private void show(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL TASKS]");
        final List<Task> tasks = service.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void showById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(task);
        System.out.println("[OK]");
    }

    @Override
    public void showByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Task task = service.updateById(id, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Task task = service.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

}
