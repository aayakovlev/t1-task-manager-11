package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectControllerImpl implements BaseController {

    private final ProjectService service;

    public ProjectControllerImpl(ProjectService service) {
        this.service = service;
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR PROJECTS]");
        service.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        final Project project = service.create(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.removeById(id);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void removeByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.removeByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    private void show(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL PROJECTS]");
        final List<Project> projects = service.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void showById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(project);
        System.out.println("[OK]");
    }

    @Override
    public void showByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        show(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Project project = service.updateById(id, name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        final Project project = service.updateByIndex(index, name, description);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

}
