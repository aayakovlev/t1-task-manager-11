package ru.t1.aayakovlev.tm.controller;

public interface BaseController {

    void clear();

    void create();

    void removeById();

    void removeByIndex();

    void showAll();

    void showById();

    void showByIndex();

    void updateById();

    void updateByIndex();

}
